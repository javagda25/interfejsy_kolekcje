package pl.sda.javagda25.zad_dom_4;

public class Trójkąt extends Wielokąt {
    private double dlugoscPodstawy;
    private double wysokosc;

    public Trójkąt(double dlugoscBokuA, double dlugoscBokuB, double dlugoscPodstawy, double wysokosc) {
        super(dlugoscBokuA, dlugoscBokuB);
        this.dlugoscPodstawy = dlugoscPodstawy;
        this.wysokosc = wysokosc;
    }

    @Override
    public double obliczPole() {
        return (dlugoscPodstawy * wysokosc) / 2;
    }

    @Override
    public double obliczObwod() {
        return dlugoscPodstawy + dlugoscBokuA + dlugoscBokuB;
    }

    @Override
    public void wypiszOpis() {
        System.out.println("Jestem sobie trójkąt: " + toString());
    }

    @Override
    public String toString() {
        return "Trójkąt{" +
                "dlugoscPodstawy=" + dlugoscPodstawy +
                ", wysokosc=" + wysokosc +
                ", dlugoscBokuA=" + dlugoscBokuA +
                ", dlugoscBokuB=" + dlugoscBokuB +
                '}';
    }
}
