package pl.sda.javagda25.zad_dom_4;

public class Kwadrat extends Prostokąt {

    public Kwadrat(double dlugoscBokuA) {
        super(dlugoscBokuA, dlugoscBokuA);
    }

    @Override
    public void wypiszOpis() {
        System.out.println("Jestem sobie kwadrat: " + toString());
    }
}
