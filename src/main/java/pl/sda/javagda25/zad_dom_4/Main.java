package pl.sda.javagda25.zad_dom_4;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String komenda;
        do {
            System.out.println("Jaki kształt? " + Arrays.toString(Kształt.values()));
            komenda = scanner.next();

            if (komenda.equalsIgnoreCase("quit")) {
                break;
            }

            try {
                Kształt kształt = Kształt.valueOf(komenda.toUpperCase());

                Figura figura;

                switch (kształt) {
                    case KOLO:
                        System.out.println("Podaj promień:");
                        double promien = scanner.nextDouble();
                        figura = new Koło(promien);
                        break;
                    case KWADRAT:
                        System.out.println("Podaj dlugosc boku A:");
                        double dlugoscBokuA = scanner.nextDouble();
                        figura = new Kwadrat(dlugoscBokuA);
                        break;
                    case TROJKAT:
                        System.out.println("Podaj dlugosc boku A:");
                        double dlugoscPodstawy = scanner.nextDouble();
                        System.out.println("Podaj dlugosc boku B:");
                        double dlugoscBokuB = scanner.nextDouble();
                        System.out.println("Podaj dlugosc boku C:");
                        double dlugoscBokuC = scanner.nextDouble();
                        System.out.println("Podaj dlugosc wysokosci:");
                        double dlugoscWysokosci = scanner.nextDouble();
                        figura = new Trójkąt(dlugoscPodstawy, dlugoscBokuB, dlugoscBokuC, dlugoscWysokosci);
                        break;
                    case PROSTOKAT:
                        System.out.println("Podaj dlugosc boku A:");
                        double dlugoscA = scanner.nextDouble();
                        System.out.println("Podaj dlugosc boku B:");
                        double dlugoscB = scanner.nextDouble();
                        figura = new Prostokąt(dlugoscA, dlugoscB);
                        break;
                    default:
                        continue;
                }

                System.out.println("Co chciałbyś policzyć?");
                komenda = scanner.next();
                if (komenda.equalsIgnoreCase("pole")) {
                    System.out.println(figura.obliczPole());
                } else if (komenda.equalsIgnoreCase("obwod")) {
                    System.out.println(figura.obliczObwod());
                } else if (komenda.equalsIgnoreCase("wypisz")) {
                    figura.wypiszOpis();
                }else{
                    System.out.println("Nie rozumiem :(");
                }

            } catch (IllegalArgumentException iae) {
                System.err.println("Błąd, nie ma takiego kształtu.");
                continue;
            }
        } while (!komenda.equalsIgnoreCase("quit"));
    }
}
