package pl.sda.javagda25.zad_dom_4;

public class Prostokąt extends Wielokąt {
    public Prostokąt(double dlugoscBokuA, double dlugoscBokuB) {
        super(dlugoscBokuA, dlugoscBokuB);
    }

    @Override
    public double obliczPole() {
        return dlugoscBokuA * dlugoscBokuB;
    }

    @Override
    public double obliczObwod() {
        return dlugoscBokuA + dlugoscBokuB + dlugoscBokuA + dlugoscBokuB;
    }

    @Override
    public void wypiszOpis() {
        System.out.println("Jestem sobie Prostokąt: " + toString());
    }
}
