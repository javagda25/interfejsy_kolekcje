package pl.sda.javagda25.zad_dom_4;

public interface Figura {
    double obliczPole();
    double obliczObwod();
    void wypiszOpis();
}
