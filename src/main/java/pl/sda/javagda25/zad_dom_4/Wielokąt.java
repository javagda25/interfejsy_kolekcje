package pl.sda.javagda25.zad_dom_4;

public abstract class Wielokąt implements Figura {
    protected double dlugoscBokuA;
    protected double dlugoscBokuB;

    public Wielokąt(double dlugoscBokuA, double dlugoscBokuB) {
        this.dlugoscBokuA = dlugoscBokuA;
        this.dlugoscBokuB = dlugoscBokuB;
    }

    @Override
    public String toString() {
        return "Wielokąt{" +
                "dlugoscBokuA=" + dlugoscBokuA +
                ", dlugoscBokuB=" + dlugoscBokuB +
                '}';
    }
}
