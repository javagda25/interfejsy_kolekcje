package pl.sda.javagda25.zad_dom_4;

public class Koło implements Figura {
    private double promien;

    public Koło(double promien) {
        this.promien = promien;
    }

    @Override
    public double obliczPole() {
        return promien * promien * Math.PI;
    }

    @Override
    public double obliczObwod() {
        return 2 * Math.PI * promien;
    }

    @Override
    public void wypiszOpis() {
        System.out.println("Jestem sobie kółko : " + toString());
    }
}
