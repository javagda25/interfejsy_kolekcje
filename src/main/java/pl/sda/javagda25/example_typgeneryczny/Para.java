package pl.sda.javagda25.example_typgeneryczny;

public class Para<TL, TR> {
    private TL lewy;
    private TR prawy;

    public Para() {
    }

    public Para(TL lewy, TR prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    public TL getLewy() {
        return lewy;
    }

    public void setLewy(TL lewy) {
        this.lewy = lewy;
    }

    public TR getPrawy() {
        return prawy;
    }

    public void setPrawy(TR prawy) {
        this.prawy = prawy;
    }

    public boolean czyNiepuste() {
        return prawy != null && lewy != null;
    }

    @Override
    public String toString() {
        return "Para{" +
                "lewy=" + lewy +
                ", prawy=" + prawy +
                '}';
    }
}
