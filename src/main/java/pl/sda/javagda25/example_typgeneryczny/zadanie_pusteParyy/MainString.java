package pl.sda.javagda25.example_typgeneryczny.zadanie_pusteParyy;

public class MainString {
    public static void main(String[] args) {
        Integer wynik = znajdz(new Integer[]{1});

        Character wynikw = zwroc('a');
    }

    public static <T> T znajdz(T[] tablica) {
        for (T t : tablica) {
            if (t != null) {
                return t;
            }
        }
        return null;
    }


    public static <G> G zwroc(G cos){
        return cos;
    }
}
