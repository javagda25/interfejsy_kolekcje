package pl.sda.javagda25.example_typgeneryczny.zadanie_pusteParyy;

import pl.sda.javagda25.example_typgeneryczny.Para;
import pl.sda.javagda25.modele.Osoba;
import pl.sda.javagda25.modele.Samochod;

public class Main {
    public static void main(String[] args) {
        Para<Osoba, Samochod> para1 = new Para<>();
        Para<Osoba, Samochod> para2 = new Para<>();
        Para<Osoba, Samochod> para3 = new Para<>();
        Para<Osoba, Samochod> para4 = new Para<>();
        Para<Osoba, Osoba>[] paras = new Para[4];
//        paras[0] = para1;
//        paras[1] = para2;
//        paras[2] = para3;
//        paras[3] = para4;

        Para<Osoba, Osoba>[] wynik = znajdzNiepuste(paras);

    }

    public static <AB, CD> Para<AB, CD>[] znajdzNiepuste(Para<AB, CD>[] pary) {
        int ilosc = 0;
        for (Para p : pary) {
            if (p.czyNiepuste()) {
                ilosc++;
            }
        }

        Para<AB, CD>[] podPary = new Para[ilosc];
        int licznikWstawiania = 0;
        for (Para p : pary) {
            if (p.czyNiepuste()) {
                podPary[licznikWstawiania++] = p;
            }
        }

        return podPary;
    }
}
