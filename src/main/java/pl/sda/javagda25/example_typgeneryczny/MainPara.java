package pl.sda.javagda25.example_typgeneryczny;

import pl.sda.javagda25.modele.Osoba;
import pl.sda.javagda25.modele.Student;

public class MainPara {
    public static void main(String[] args) {
        Student jacek = new Student("Jacek", "Kowalski");
        Student gosia = new Student("Gosia", "Nowak");
        Osoba janek = new Osoba("w");
        Para<Student, Osoba> para = new Para<>(jacek, janek);

        Student lewy = para.getLewy();

        Object o = para.getPrawy();
        if(o instanceof Student){
            Student rzutowany = (Student) o;
            rzutowany.getImie();
        }
    }
}
