package pl.sda.javagda25.example_typgeneryczny;

import java.util.Optional;

public class MainOptionalDobrze {
    public static void main(String[] args) {
        Integer[] tablicaWartosci = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 10};
        Optional zwroconaWartosc = znajdzWartosc100(tablicaWartosci);

        if (zwroconaWartosc.isPresent()) {
            Object wartosc = zwroconaWartosc.get();// wyciągnij z pudełka (wypakuj)
            if(wartosc instanceof Integer) {
                Integer wartoscInteger = (Integer) wartosc;

                System.out.println(wartoscInteger);
                double rzutowana = wartoscInteger.doubleValue();
            }
        }
    }

    /**
     * Metoda szuka w tablicy wartosi 100 i zwraca referencję.
     *
     * @param tablicaWartosci - tablica wartości.
     * @return refrencję na obiekt lub null jeśli go nie znajdzie.
     */
    private static Optional znajdzWartosc100(Integer[] tablicaWartosci) {
        for (Integer wartosc : tablicaWartosci) {
            if (wartosc == 100) {
                return Optional.ofNullable(wartosc); // pełne pudełko
            }
        }
        return Optional.empty(); // puste pudełko
    }
}
