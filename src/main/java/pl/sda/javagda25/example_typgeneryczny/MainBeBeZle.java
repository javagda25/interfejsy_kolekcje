package pl.sda.javagda25.example_typgeneryczny;

public class MainBeBeZle {
    public static void main(String[] args) {
        Integer[] tablicaWartosci = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 10};
        Integer zwroconaWartosc = znajdzWartosc100(tablicaWartosci);

        if (zwroconaWartosc != null) {
            System.out.println(zwroconaWartosc);
            double rzutowana = zwroconaWartosc.doubleValue();
        }
    }

    /**
     * Metoda szuka w tablicy wartosi 100 i zwraca referencję.
     *
     * @param tablicaWartosci - tablica wartości.
     * @return refrencję na obiekt lub null jeśli go nie znajdzie.
     */
    private static Integer znajdzWartosc100(Integer[] tablicaWartosci) {
        for (Integer wartosc : tablicaWartosci) {
            if (wartosc == 100) {
                return wartosc;
            }
        }
        return null;
    }
}
