package pl.sda.javagda25.example_typgeneryczny.zadanie_pudelko;

import pl.sda.javagda25.modele.Samochod;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
//        LocalDate date; // do zmiany javy na 8
        Pudelko<Samochod> samochodPudelko = new Pudelko<>();
        System.out.println(samochodPudelko.czyPudelkoJestPuste());
        samochodPudelko.setObiekt(new Samochod("Crysler", "Czerwony"));
        System.out.println(samochodPudelko.czyPudelkoJestPuste());

        Pudelko pudelko = new Pudelko(); // typu Object

        System.out.println(pudelko.getObiekt());

    }
}
