package pl.sda.javagda25.example_typgeneryczny.zadanie_pudelko;

import pl.sda.javagda25.zad_dom_1.IOpenable;
import pl.sda.javagda25.zad_dom_1.IZamykalny;

public class Pudelko<T> {
    private T obiekt;

    public Pudelko() {
    }

    public Pudelko(T obiekt) {
        this.obiekt = obiekt;
    }

    public T getObiekt() {
        return obiekt;
    }

    public void setObiekt(T obiekt) {
        this.obiekt = obiekt;
    }

    public boolean czyPudelkoJestPuste() {
        return obiekt == null; // true to puste
    }

    @Override
    public String toString() {
        return "Pudelko{" +
                "obiekt=" + obiekt +
                '}';
    }
}
