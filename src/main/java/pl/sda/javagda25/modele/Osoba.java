package pl.sda.javagda25.modele;

import java.util.Objects;

public class Osoba implements Comparable<Osoba>{
    private String pesel, imie, nazwisko;

    public Osoba(String pesel) {
        this.pesel = pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Osoba)) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(pesel, osoba.pesel) &&
                Objects.equals(imie, osoba.imie) &&
                Objects.equals(nazwisko, osoba.nazwisko);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pesel, imie, nazwisko);
    }

    @Override
    public int compareTo(Osoba o) {
        return o.pesel.compareTo(pesel);
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "pesel='" + pesel + '\'' +
                '}';
    }
}
