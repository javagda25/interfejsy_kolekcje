package pl.sda.javagda25.modele;

public class Samochod {
    private String marka;
    private String kolor;

    public Samochod(String marka, String kolor) {
        this.marka = marka;
        this.kolor = kolor;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "marka='" + marka + '\'' +
                ", kolor='" + kolor + '\'' +
                '}';
    }
}
