package pl.sda.javagda25.zad_dom_1;

import pl.sda.javagda25.zad_dom_1.models.Brama;
import pl.sda.javagda25.zad_dom_1.models.Drzwi;
import pl.sda.javagda25.zad_dom_1.models.Okno;

public class Main {
    public static void main(String[] args) {
        IOpenable[] openables = new IOpenable[10];
        openables[0] = new Drzwi();
        openables[1] = new Okno();
        openables[2] = new Drzwi();
        openables[3] = new Brama();
        openables[4] = new Drzwi();
        openables[5] = new Okno();
        openables[6] = new Okno();
        openables[7] = new Brama();
        openables[8] = new Okno();
        openables[9] = new Drzwi();

        for (int i = 0; i < openables.length; i++) { // fori
            IOpenable openable = openables[i];

            if (openable.czyOtwarty()) {
                System.out.println("Otwarty");
            }
        }

        // dla każdego elementu typu IOpenable o nazwie 'openable' z kolekcji 'openables'
        for (IOpenable nazwaZmiennej : openables) {  // for each (dla każdego elementu)

            if (nazwaZmiennej.czyOtwarty()) {
                System.out.println("Otwarty");
            }

            if (nazwaZmiennej instanceof IOtwieralny) {
                IOtwieralny iOtwieralny = (IOtwieralny) nazwaZmiennej;
                iOtwieralny.otworz();
            }
        }
    }
}
