package pl.sda.javagda25.zad_dom_1;

public interface IOpenable {
    boolean czyOtwarty();
}
