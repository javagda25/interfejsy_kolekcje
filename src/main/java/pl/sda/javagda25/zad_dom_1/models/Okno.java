package pl.sda.javagda25.zad_dom_1.models;

import pl.sda.javagda25.zad_dom_1.IOtwieralny;
import pl.sda.javagda25.zad_dom_1.IZamykalny;

public class Okno implements IZamykalny, IOtwieralny {
    private boolean czyOtwarte;
    @Override
    public void otworz() {
        czyOtwarte = true;
    }

    @Override
    public void zamknij() {
        czyOtwarte = false;
    }

    @Override
    public boolean czyOtwarty() {
        return czyOtwarte;
    }
}
