package pl.sda.javagda25.zad_dom_1;

public interface IWłączalny {
    void włącz();
    boolean czyWłączony();
}
