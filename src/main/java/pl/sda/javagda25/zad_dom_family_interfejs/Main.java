package pl.sda.javagda25.zad_dom_family_interfejs;

public class Main {
    public static void main(String[] args) {
        ICzlonekRodziny[] czlonkowieRodziny = new ICzlonekRodziny[5];
        czlonkowieRodziny[0] = new Corka("Ala");
        czlonkowieRodziny[1] = new Syn("Janek");
        czlonkowieRodziny[2] = new Matka("Ola");
        czlonkowieRodziny[3] = new Ojciec("Władek");
        czlonkowieRodziny[4] = new Ciocia("Janka");

        for (ICzlonekRodziny iCzlonekRodziny : czlonkowieRodziny) {
            iCzlonekRodziny.przedstawSie();
        }
    }

    private static <V> Ojciec[] znajdzOjcow(V[] ojcowie) {
        int licznik = 0;
        for (V v : ojcowie) {
            if (v instanceof Ojciec) {
                licznik++;
            }
        }

        Ojciec[] wynik = new Ojciec[licznik];
        int pozycjaWstawiania = 0;
        for (V v : ojcowie) {
            if (v instanceof Ojciec) {
                wynik[pozycjaWstawiania++] = (Ojciec) v;
            }
        }
        return wynik;
    }
}
