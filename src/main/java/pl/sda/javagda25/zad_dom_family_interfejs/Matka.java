package pl.sda.javagda25.zad_dom_family_interfejs;

public class Matka extends ICzlonekRodziny {
    public Matka(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am mother");
    }

    @Override
    public boolean jestDorosly() {
        return true;
    }
}
