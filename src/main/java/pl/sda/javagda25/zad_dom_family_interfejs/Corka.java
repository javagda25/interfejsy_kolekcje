package pl.sda.javagda25.zad_dom_family_interfejs;

public class Corka extends ICzlonekRodziny {
    public Corka(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("jestem corka");
    }

    @Override
    public boolean jestDorosly() {
        return false;
    }
}
