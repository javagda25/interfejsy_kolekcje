package pl.sda.javagda25.zad_dom_family_interfejs;

public class Ciocia extends ICzlonekRodziny {
    public Ciocia(String name) {
        super(name);
    }

    @Override
    public boolean jestDorosly() {
        return true;
    }
}
