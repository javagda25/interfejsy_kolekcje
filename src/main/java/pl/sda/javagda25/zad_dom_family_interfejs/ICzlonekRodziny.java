package pl.sda.javagda25.zad_dom_family_interfejs;

public abstract class ICzlonekRodziny {
    private String name;

    public ICzlonekRodziny(String name) {
        this.name = name;
    }

    public void przedstawSie(){
        System.out.println("Jestem czlonkiem rodziny");
    }

    public abstract boolean jestDorosly();
}
