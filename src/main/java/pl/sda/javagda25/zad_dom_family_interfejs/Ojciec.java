package pl.sda.javagda25.zad_dom_family_interfejs;

public class Ojciec extends ICzlonekRodziny {
    public Ojciec(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am your father");
    }

    @Override
    public boolean jestDorosly() {
        return true;
    }
}
