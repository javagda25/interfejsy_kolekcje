package pl.sda.javagda25.zad_dom_family_interfejs;

public class Syn extends ICzlonekRodziny {
    public Syn(String name) {
        super(name);
    }

    @Override
    public void przedstawSie() {
        System.out.println("who's asking?");
    }

    @Override
    public boolean jestDorosly() {
        return false;
    }
}
