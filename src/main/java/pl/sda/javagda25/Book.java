package pl.sda.javagda25;

public class Book {
    private String[] autorzy = new String[3];
    private int iloscAutorow = 0;

    public Book() {
    }

    public void dodajAutora(String autor) {
        autorzy[iloscAutorow++] = autor;
    }

    public void wypiszAutorow() {
        for (int i = 0; i < iloscAutorow; i++) {
            System.out.println(autorzy[i]);
        }
    }
}
