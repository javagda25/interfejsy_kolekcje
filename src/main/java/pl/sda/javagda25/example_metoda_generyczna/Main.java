package pl.sda.javagda25.example_metoda_generyczna;

import pl.sda.javagda25.modele.Osoba;

public class Main {
    public static void main(String[] args) {
        System.out.println(znajdzWTablicy(new Integer[]{1, 2, 3, 4, 5, 6}, 8));
        System.out.println(znajdzWTablicy(new Integer[]{1, 2, 3, 4, 5, 6}, 4));


        System.out.println(znajdzWTablicy(new Double[]{1d, 2d, 3d, 4.0, 5d, 6d}, 4.0));
        System.out.println(znajdzWTablicy(new Double[]{1d, 2d, 3d, 4d, 5d, 6d}, 8d));


        System.out.println(znajdzWTablicy(new Osoba[]{
                new Osoba("a"),
                new Osoba("b"),
                new Osoba("c")
        }, new Osoba("d")));

        System.out.println(znajdzWTablicy(new Osoba[]{
                new Osoba("a"),
                new Osoba("b"),
                new Osoba("c")
        }, new Osoba("a")));
    }

    private static <T> int znajdzWTablicy(T[] tablica, T szukany) { // indexOf
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i].equals(szukany)) {
                return i;
            }
        }
        return -1; // nie ma indeksu -1
    }

    private static <T extends Number> Double zsumujLiczby(T liczbaA, T liczbaB) { // indexOf
        return liczbaA.doubleValue() + liczbaB.doubleValue();
    }
}
