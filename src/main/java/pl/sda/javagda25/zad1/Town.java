package pl.sda.javagda25.zad1;

public class Town {
    private Citizen[] obywatele;

    public Town(Citizen[] obywatele) {
        this.obywatele = obywatele;
    }

    public void setObywatele(Citizen[] obywatele) {
        this.obywatele = obywatele;
    }

    public int howManyCanVote() {
        int licznik = 0;

        for (int i = 0; i < obywatele.length; i++) {
            if (obywatele[i].canVote()) {
                licznik++;
            }
        }
        return licznik;
    }
}
