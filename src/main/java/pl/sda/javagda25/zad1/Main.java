package pl.sda.javagda25.zad1;

public class Main {
    public static void main(String[] args) {
        // tworzę obywateli
        Citizen[] tablica = new Citizen[7];
        // uzupełniamy tablice
        tablica[0] = new Soldier("a");
        tablica[1] = new King("b");
        tablica[2] = new Peasant("c");
        tablica[3] = new Peasant("d");
        tablica[4] = new Soldier("e");
        tablica[5] = new Peasant("f");
        tablica[6] = new Townsman("g");

        Town town = new Town(tablica);
        System.out.println("Ile? " + town.howManyCanVote());
//        town.setObywatele(tablica);
        // od teraz miasto ma obywateli
    }
}
