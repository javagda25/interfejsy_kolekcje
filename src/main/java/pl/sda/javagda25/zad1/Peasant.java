package pl.sda.javagda25.zad1;

public class Peasant extends Citizen{
    public Peasant(String name) {
        super(name);
    }

    public boolean canVote() {
        return false;
    }
}
