package pl.sda.javagda25.zad1;

public class Townsman extends Citizen {
    public Townsman(String name) {
        super(name);
    }

    public boolean canVote() {
        return true;
    }
}
