package pl.sda.javagda25.zad1;

public class Soldier extends Citizen {
    public Soldier(String name) {
        super(name);
    }

    public boolean canVote() {
        return true;
    }
}
