package pl.sda.javagda25.zad1;

public abstract class Citizen {
    private String name;

    public Citizen(String name) {
        this.name = name;
    }

    public abstract boolean canVote();

}
