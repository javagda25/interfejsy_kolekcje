package pl.sda.javagda25.zad2;

public interface IDrzwi {
    // interfejs nie może mieć pól
    public static final int a = 5;

    // interfejs ma metody tylko abstrakcyjne
    public abstract void otworz();
    public abstract void zamknij();
}
