package pl.sda.javagda25.exampleCollections.set.zadanie_domowe1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<Integer> zbior = new TreeSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
        System.out.println(zbior);
        System.out.println(zbior.size());

        zbior.remove(10);
        zbior.remove(12);

        System.out.println(zbior);
    }

    private static boolean zawieraDuplikaty(String tekst) {
        // dzielimy cały tekst na literki (usuwamy spacje)
        // replaceAll(" ", "") - usunięcie spacji
        // split("") - podziel na literki (dziel 'po niczym')
        String[] wynik = tekst.replaceAll(" ", "").split("");

        // dodanie wszystkich literek (w postaci String) do kolekcji
//        List<String> literki = new ArrayList<>(Arrays.asList(wynik));

        // dodanie wszystkich literek do setu (usunięcie duplikatów)
        Set<String> literkiUnikalne = new HashSet<>(Arrays.asList(wynik));

        // porównanie rozmiarów (set nie zawiera duplikatów, lista tak)
        // jeśli różnią się wielkością, to znaczy że jakiś duplikat
        // został usunięty.
        return wynik.length != literkiUnikalne.size();
    }
}
