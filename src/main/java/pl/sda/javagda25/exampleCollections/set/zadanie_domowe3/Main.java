package pl.sda.javagda25.exampleCollections.set.zadanie_domowe3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<ParaLiczb> paraLiczbSet = new HashSet<>();

        paraLiczbSet.add(new ParaLiczb(1, 2));
        paraLiczbSet.add(new ParaLiczb(2, 1));
        paraLiczbSet.add(new ParaLiczb(1, 1));
        paraLiczbSet.add(new ParaLiczb(1, 2));

        System.out.println(paraLiczbSet);
    }
}
