package pl.sda.javagda25.exampleCollections.set;

import pl.sda.javagda25.modele.Osoba;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        // S - Single responsibility
        // Klasa / metoda powinna mieć wyłącznie jedną odpowiedzialność

        // O - Open Close (principal)
        // Kod powinien być otwarty na rozszerzenia i zamknięty na modyfikacje

        // L - Liskov Substitution

        Osoba o = new Osoba("a");
        Osoba oc = new Osoba("a");
        Set<Osoba> zbior = new HashSet<>(Arrays.asList(o, oc));

        Osoba b = new Osoba("A");

        System.out.println(o.equals(b));
        System.out.println(oc.equals(o));
        System.out.println(o.hashCode());
        System.out.println(oc.hashCode());

        zbior = new HashSet<>(Arrays.asList(o, oc, b));

        System.out.println(zbior);
    }
}
