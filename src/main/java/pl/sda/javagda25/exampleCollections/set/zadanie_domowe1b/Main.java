package pl.sda.javagda25.exampleCollections.set.zadanie_domowe1b;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String tekst = scanner.nextLine();
        Set<String> unikalneLitery = findUnique(tekst);
        System.out.println(unikalneLitery);

        Set<Character> unikalneLitery2 = alternatywneRozwiazanie(tekst);

        System.out.println(unikalneLitery2);
    }

    private static Set<String> findUnique(String tekst) {
        String[] wynik = tekst.replaceAll(" ", "").split("");

        // wszystkie litery z usunięciem duplikatów
        List<String> wszystkieLiterki = new ArrayList<>(Arrays.asList(wynik));

        Set<String> unikalne = new HashSet<>(wszystkieLiterki);

        List<String> nieUnikalne = new ArrayList<>(wszystkieLiterki);
        // a a a b c d

//        nie moge zastosować, ponieważ usuwa wszystkie wystąpienia każdej litery.
//        a interesuje mnie usunięcie wyłącznie jednego egzemplarza
//        nieUnikalne.removeAll(unikalne);

        // usuwam po jednym egzemplarzu każdej litery która wystąpiła
        // z listy
        //      a a a b c d (nieunikalne)
        //    - a b c d     (unikalne)
        //     -------------
        //      a a         (wynik jest w nieUnikalne)
        for (String litera: unikalne) {
            nieUnikalne.remove(litera); // nieunikalne = 'a, a'
        }

        wszystkieLiterki.removeAll(nieUnikalne);

        return new HashSet<>(wszystkieLiterki);
    }

    public static Set<Character> alternatywneRozwiazanie(String text){
        Set<Character> result = new HashSet<>();
        Set<Character> redundant = new HashSet<>();

        char[] split = text.toCharArray();

        for (char c : split) {
            if (result.contains(c)){
                redundant.add(c);
            }
            else {
                result.add(c);
            }

        }
        result.removeAll(redundant);

        return result;
    }

}
