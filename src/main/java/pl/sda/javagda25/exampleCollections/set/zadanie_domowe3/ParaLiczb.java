package pl.sda.javagda25.exampleCollections.set.zadanie_domowe3;

import java.util.Objects;

public class ParaLiczb {
    private int a, b;

    public ParaLiczb(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParaLiczb)) return false;
        ParaLiczb paraLiczb = (ParaLiczb) o;
        return a == paraLiczb.a &&
                b == paraLiczb.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
