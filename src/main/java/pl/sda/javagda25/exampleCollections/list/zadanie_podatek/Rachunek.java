package pl.sda.javagda25.exampleCollections.list.zadanie_podatek;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {
    private List<Produkt> produkts = new ArrayList<>();

    public Rachunek() {
    }

    public void dodajProdukt(Produkt doDodania) {
        produkts.add(doDodania);
    }

    public void wypiszRachunek() {
        System.out.println("Rachunek: ");
        for (Produkt pr : produkts) {
            System.out.println(pr);
        }
        System.out.println("Suma wartości: " + podsumujRachunekBrutto());
    }

    public double podsumujRachunekBrutto() {
        double suma = 0.0;
        for (Produkt pr : produkts) {
            suma += pr.podajCeneBrutto();
        }
        return suma;
    }

    public double podsumujRachunekNetto() {
        double suma = 0.0;
        for (Produkt pr : produkts) {
            suma += pr.getCenaNetto();
        }
        return suma;
    }

    public double zwrocWartoscPodatku() {
        return podsumujRachunekBrutto() - podsumujRachunekNetto();
    }

}
