package pl.sda.javagda25.exampleCollections.list.zadanie_podatek;

public class Main {
    public static void main(String[] args) {
        Produkt p1 = new Produkt("a", 1.0, PodatekProduktu.VAT8);
        Produkt p2 = new Produkt("b", 10.0, PodatekProduktu.VAT8);
        Produkt p3 = new Produkt("c", 5.0, PodatekProduktu.NO_VAT);
        Produkt p4 = new Produkt("d", 50.0, PodatekProduktu.VAT23);
        Produkt p5 = new Produkt("e", 100.0, PodatekProduktu.VAT5);
        Produkt p6 = new Produkt("f", 300.0, PodatekProduktu.VAT8);

        Rachunek rachunek = new Rachunek();
        rachunek.dodajProdukt(p1);
        rachunek.dodajProdukt(p2);
        rachunek.dodajProdukt(p3);
        rachunek.dodajProdukt(p4);
        rachunek.dodajProdukt(p5);
        rachunek.dodajProdukt(p6);

        System.out.println(rachunek.podsumujRachunekNetto());
        System.out.println(rachunek.podsumujRachunekBrutto());
        System.out.println(rachunek.zwrocWartoscPodatku());

        rachunek.wypiszRachunek();

    }
}
