package pl.sda.javagda25.exampleCollections.list;

import pl.sda.javagda25.modele.Osoba;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // SOLID
        // S
        // O
        // L - Zasada Liskov - Liskov substitution
        // I
        // D
        List<String> lista = new LinkedList<>();

        lista.add("1");
        lista.add("2");
        lista.add(null);
        lista.add(null);
        lista.add("5");

        System.out.println(lista);
        lista.remove(1);

        System.out.println(lista); // 1, 3, 4, 5
        metoda(lista);
    }
    private static void metoda(List<String> m){

    }
}
