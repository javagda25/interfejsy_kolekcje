package pl.sda.javagda25.exampleCollections.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainString {
    public static void main(String[] args) {
        // SOLID
        // S
        // O
        // L - Zasada Liskov - Liskov substitution
        // I
        // D
        List<Integer> lista = new ArrayList<>();

        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);

        System.out.println(lista);
        lista.remove((Integer)1);

        System.out.println(lista); //
        lista.add(1, 100);
        System.out.println(lista); //
        lista.set(1, 200);
        System.out.println(lista); //

        List<Integer> kopia = new ArrayList<>(lista);// kopia listy
        Collections.sort(lista); // sortowanie kolekcji

        System.out.println(lista);
    }
}
