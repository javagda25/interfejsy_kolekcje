package pl.sda.javagda25.exampleCollections.list.zadanie_podatek;

public class Produkt {
    private String nazwa;
    private double cenaNetto;
    private PodatekProduktu podatek;

    public Produkt(String nazwa, double cenaNetto, PodatekProduktu podatek) {
        this.nazwa = nazwa;
        this.cenaNetto = cenaNetto;
        this.podatek = podatek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    public PodatekProduktu getPodatek() {
        return podatek;
    }

    public void setPodatek(PodatekProduktu podatek) {
        this.podatek = podatek;
    }

    public double podajCeneBrutto() {
        //     100       * (1 + 0.23)
        //     100       * (1.23)
        return cenaNetto * (1 + podatek.getWartoscPodatku());
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", cenaNetto=" + cenaNetto +
                ", podatek=" + podatek +
                '}';
    }
}
