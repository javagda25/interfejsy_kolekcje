package pl.sda.javagda25.exampleCollections.list;

import pl.sda.javagda25.modele.Osoba;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
//import java.awt.List;

public class MainOsoba {
    public static void main(String[] args) {
        // SOLID
        // S
        // O
        // L - Zasada Liskov - Liskov substitution
        // I
        // D
        List<Osoba> lista = new ArrayList<>();

        lista.add(new Osoba("a"));
        lista.add(new Osoba("b"));
        lista.add(new Osoba("c"));
        lista.add(new Osoba("d"));
        lista.add(new Osoba("e"));

        System.out.println(lista);
        lista.remove(1);

        List<Osoba> kopia = new ArrayList<>(lista);// kopia listy
        Collections.sort(lista); // sortowanie kolekcji

        System.out.println(lista); // 1, 3, 4, 5
        System.out.println(kopia); // 1, 3, 4, 5

        kopia.get(0).setPesel("blabla");
        System.out.println(lista); // 1, 3, 4, 5
        System.out.println(kopia); // 1, 3, 4, 5

        Collections.shuffle(lista);
        System.out.println(lista); // 1, 3, 4, 5



    }
}
