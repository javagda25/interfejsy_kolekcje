package pl.sda.javagda25.exampleCollections.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainKonstKop {
    public static void main(String[] args) {
//        List<String> listaStringow = Arrays.asList("a", "b", "c", "d");
        List<String> listaStringow = new ArrayList<>(Arrays.asList("a", "b", "c", "d"));
//        List<String> listaStringow = new ArrayList<>(Arrays.asList(new String[]{"a", "b", "c", "d"}));

        System.out.println(listaStringow);
        listaStringow.add("g");
        System.out.println(listaStringow);

    }
}
